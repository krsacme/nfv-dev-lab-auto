def dir_name = "OSP-Setup"
if (JOB_NAME != "import_job") {
  dir_name = JOB_NAME + "_OSP_Setup"
}
queue(dir_name + '/Baremetal_Install')
queue(dir_name + '/FFU_13_16')
queue(dir_name + '/OSP_Install')
queue(dir_name + '/OSP_Update')
queue(dir_name + '/Perf')
queue(dir_name + '/Tempest')
queue(dir_name + '/OSP_OCP_Deploy')
