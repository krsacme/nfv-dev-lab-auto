# nfv-dev-lab-auto

This repo contains the JOB definitions and the scripts to run jobs. The jobs
are created dynamically when this repo is imported by a Freestyle Jenkins Job.
The below sections provides steps to create the import job which will intern
create (import) all the required pipeline jobs. Additional credentials are required to in order to use the beaker, see below for additing it.

## Creating an Import JOB

1. In the Jenkins, Choose `New Item`
2. Enter an item name like `import job`
3. Choose `Freestyle project`
4. Complete job creating by presssing `OK`
5. Under the section `Source Code Management`, choose the `Git` option
6. In the `Repository URL` input section, provide the URL of the repo, which is `git@github.com:rh-nfv-int/nfv-dev-lab-auto.git`
7. Since this repo is private, choose the crendentials as `jenkins`  (if `jenkins` credentials is not present create on with the private key of the jenkins ssh key). This key should be added to the github repo as [deploy keys](https://github.com/rh-nfv-int/nfv-dev-lab-auto/settings/keys) to allow it to access.
8. Under the `Build` section, choose `Add build step` and choose `Process DSL Jobs` option
9. With the `Look on Filesystem` option selected, provid the `DSL Scripts` value as `main.groovy`
10. Repeat step 8 to create another step
9. With the `Look on Filesystem` option selected, provid the `DSL Scripts` value as `trigger.groovy`
10. Save the job and build it

NOTE: Importing the job is not cleaning the cloned directory which results in update not visible on the jenkins. Execute below command remove the directories on jenkins server
```
sudo rm -rf $(find /var/lib/jenkins/workspace/OSP-Setup/ -name '*script*' | xargs)
```

## Beaker User changes

1. Create a credentials in the Jenkins server with beaker user name
   and password. Keep the ID same as the beaker user name for
   convinient usage.
   * Open link to create new [credentials](http://nfv-int-dev.usersys.redhat.com:8080/credentials/store/system/domain/_/newCredentials)
   * Choose kind as `Username with password`
   * Enter User name as your kerberos username (used o login [beaker](https://beaker.engineering.redhat.com/) server
   * Enter password as your kerneros password
   * Enter ID same as username above
   * Press `OK` to create

2. Your should a member of the beaker group [rhos-nfv-blr](https://beaker.engineering.redhat.com/groups/rhos-nfv-blr#members) to access the team machines. If you are not part of this group contact Yogi to be added to this group

3. Copy the Jenkins (`tools/jenkins.pub`) key from the Jenkins server and add it
   to the beaker server SSH Public keys
   * Open link to add ssh key to [beaker](https://beaker.engineering.redhat.com/prefs/#ssh-public-keys)
   * Add the ssh public key of jenkins server (provided below)
   * Also add your own public key to access the provisioned machine from your laptop (~/.ssh/id_rsa.pub)

```
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDaUrV8bEPQ84y2Y+gaTLrAW/1zDL0wg667SRFZH8K1lUHRcpHY67SzXrqxoLz4LOoR2k0AkBV3iPsR/bgUqY86h1tLSulUcnl8zzLALwXX/sID0MB8msB+D3l0ArBisamesJV3LYXrBv8Cm1y+K4bsTN287lkBKl4ah1qa5FUtiInc1dTzRq0g6shjDEg807fIbaoNBavu14tT5j1sUKudIiTrn2rlZIt/Dtn2desIjaUKmSMMCI6zThAe7i0+b0ewyn2QwZh0OZwAE3PbzE9ha5INoBs+t/nSgm1lMBOb40Scm993WTSN0ForA7VrGY+VZkN+86/JnMuwrh3rSK9d jenkins@jenk4
```

## Template Directory

* Github URL of the template directory should be provided with OSP deployment
* The directory inside the repo should be of the format `OSP<MajorVersion>_ref`.
  `<MajorVersion>` is 13 or 16 based on the version to be deployed. The valid
  folder names are `OSP13_ref` or `OSP16_ref`.
* `network_data.yaml` is not used from the template directory as it is part
  of this repository
* VLAN ranges will be modified as per the cluster in the file
  `network-environment.yaml` as per the cluster configuration configured in
  this repo.
* The deploy commands should be of the format `overcloud_deploy_<type>.sh`.
  The `<type>` can be `regular` or `offload` or `nicpart` with corresponding
  feature's deploy command.
* Sample Repositories:

    * [Team's Repo](https://github.com/rh-nfv-int/osp-dev-lab-tht.git)
    * [Yogi's Repo](https://github.com/yogananth-subramanian/tht-dpdk)
    * [Saravanan's Repo](https://github.com/krsacme/tht-dpdk)

