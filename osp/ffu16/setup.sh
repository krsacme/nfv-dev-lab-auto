#!/bin/bash

set -ex

cd /root/infrared
source .venv/bin/activate

infrared plugin remove tripleo-upgrade
infrared plugin add --revision stable/train tripleo-upgrade
if [ -n "" ]; then
    pushd plugins/tripleo-upgrade
    git remote add gerrit https://review.opendev.org/openstack/tripleo-upgrade.git
    for a_patch in
        do
            git review -x $a_patch
        done
    popd
fi

# use mv instead of symbolic link to avoid too many levels of symbolic links issue
mkdir -p $(pwd)/plugins/tripleo-upgrade/infrared_plugin/roles/tripleo-upgrade
find $(pwd)/plugins/tripleo-upgrade -maxdepth 1 -mindepth 1 -not -name infrared_plugin -exec mv '{}' $(pwd)/plugins/tripleo-upgrade/infrared_plugin/roles/tripleo-upgrade \;

sudo yum install patch -y

if [ ! -f workarounds.yaml ]; then
    curl -sSko workarounds.yaml https://gitlab.cee.redhat.com/rhos-upgrades/workarounds/-/raw/master/upgrade/13-16.1_upgrade_workarounds.yaml
    sed -i -E 's|(.*rhos-release 16.1.*)-p [A-Za-z0-9_.-]+|\1-p passed_phase2|' workarounds.yaml
    sed -i  's/redhat\.local/localdomain/g' workarounds.yaml
fi

