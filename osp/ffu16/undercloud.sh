#!/bin/bash

set -ex

cd /root/infrared
source .venv/bin/activate

# Takes 30mins
infrared tripleo-upgrade --undercloud-ffu-os-upgrade yes --upgrade-ffu-workarounds true -e @workarounds.yaml -e leapp_unsubscribed=True -e leapp_skip_release_check=True

# Takes 7mins
infrared tripleo-undercloud -o undercloud-upgrade.yml --upgrade yes --mirror "tlv" --build passed_phase2   --ansible-args="tags=discover_python,upgrade_repos,undercloud_containers" --version 16.1 -e undercloud_version=13

# Takes 46 mins
infrared tripleo-upgrade --undercloud-ffu-upgrade yes --undercloud-ffu-releases '14,15,16.1' --mirror "tlv" --upgrade-ffu-workarounds true -e @workarounds.yaml


infrared ssh undercloud-0 'cat << EOF > /home/stack/overcloud-repos.yaml
parameter_defaults:
  UpgradeInitCommand: |
    sudo yum localinstall -y http://rhos-release.virt.bos.redhat.com/repos/rhos-release/rhos-release-latest.noarch.rpm
    sudo rhos-release -x
    sudo yum clean all
    sudo rhos-release 16.1

    # TODO(skramaja): Remove once the patch is merged
    if [ ! -f patched ]; then
        sudo dnf install os-net-config -y
        curl -4 https://review.opendev.org/changes/openstack%2Fos-net-config~770230/revisions/1/patch?download | base64 -d | sudo patch -d /usr/lib/python3.6/site-packages/ -p1
        touch patched
    fi
EOF'

# https://review.opendev.org/c/openstack/tripleo-heat-templates/+/770184
infrared ssh undercloud-0 'curl -4 https://review.opendev.org/changes/openstack%2Ftripleo-heat-templates~770184/revisions/2/patch?download | base64 -d | sudo patch -d /usr/share/openstack-tripleo-heat-templates/ -p1'

# https://review.opendev.org/c/openstack/tripleo-heat-templates/+/770268
infrared ssh undercloud-0 'curl -4 https://review.opendev.org/changes/openstack%2Ftripleo-heat-templates~770268/revisions/1/patch?download | base64 -d | sudo patch -d /usr/share/openstack-tripleo-heat-templates/ -p1'
