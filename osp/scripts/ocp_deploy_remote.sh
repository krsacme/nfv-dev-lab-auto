#!/bin/bash
###########################################################
# This script runs on the hypervisor node ################
###########################################################

set -ex

if [ ! -d ~/sos-fdp ]; then
    git clone https://github.com/krsacme/sos-fdp.git
fi

cd sos-fdp

pip3 install yq --user
source ~/overcloudrc

EXT_CIDR=$(yq '.[] | select(.name == "External") | .ip_subnet' ~/osp16_ref/network_data.yaml | sed 's/"//g')
EXT_PREFIX=${EXT_CIDR%.*}
if ! openstack network show public 2>1 >/dev/null; then
    openstack network create public --provider-network-type flat --provider-physical-network datacentre --external
    openstack subnet create public --subnet-range ${EXT_PREFIX}.0/24 --network public --allocation-pool start=${EXT_PREFIX}.120,end=${EXT_PREFIX}.130 --dns-nameserver ${EXT_PREFIX}.1 --no-dhcp
fi

FIP=$(openstack floating ip list --tag ocpapi -c 'Floating IP Address' -f value)
if [ -z ${FIP} ]; then
    openstack floating ip create public --description "OCP API" --tag ocpapi
    FIP=$(openstack floating ip list --tag ocpapi -c 'Floating IP Address' -f value)
fi
if [ -z ${FIP} ]; then
    echo "Floating ip is not created, exiting..."
    exit 1
fi

sed -i -E 's/^( *lbFloatingIP: )(.*)$/\1'${FIP}'/' install-config.yaml
sed -i -E 's/^( *externalDNS: ).*$/\1["'${EXT_PREFIX}'.1"]/' install-config.yaml
sed -i '/  ssh-rsa.*/d' install-config.yaml
printf "  %s" "$(cat ~/.ssh/id_rsa.pub)">>install-config.yaml

OCP=4.7
if [ ! -f /usr/local/bin/openshift-install ]; then
    wget -q --retry-connrefused --waitretry=1 --read-timeout=20 --timeout=15 -t 0 https://mirror.openshift.com/pub/openshift-v4/clients/ocp/stable-${OCP}/openshift-install-linux.tar.gz
    tar xvf openshift-install-linux.tar.gz && sudo mv openshift-install /usr/local/bin
    rm -rf openshift-install-linux.tar.gz
fi

if [ ! -f /usr/local/bin/oc ]; then
    wget -q --retry-connrefused --waitretry=1 --read-timeout=20 --timeout=15 -t 0 https://mirror.openshift.com/pub/openshift-v4/clients/ocp/stable-${OCP}/openshift-client-linux.tar.gz
    tar xvf openshift-client-linux.tar.gz && sudo mv kubectl oc /usr/local/bin
    rm -rf openshift-client-linux.tar.gz
fi

OV_AUTH=$(yq .clouds.overcloud.auth.auth_url ~/.config/openstack/clouds.yaml | sed 's/"//g')
[[ "$OV_AUTH" == "null" ]] && exit 1 || echo $OV_AUTH
cp clouds.yaml /tmp/clouds1.yaml
sed -i '/password/d' /tmp/clouds1.yaml
sed -i '/auth_url/d' /tmp/clouds1.yaml
yq -y ".clouds.openstack.auth += {\"auth_url\" : \"$OV_AUTH\" }" /tmp/clouds1.yaml > /tmp/clouds2.yaml
yq -y ".clouds.overcloud.auth += {\"auth_url\" : \"$OV_AUTH\" }" /tmp/clouds2.yaml > /tmp/clouds3.yaml

OV_PASS=$(yq .clouds.overcloud.auth.password ~/.config/openstack/clouds.yaml | sed 's/"//g')
yq -y ".clouds.openstack.auth += {\"password\" : \"$OV_PASS\" }" /tmp/clouds3.yaml > /tmp/clouds4.yaml
yq -y ".clouds.overcloud.auth += {\"password\" : \"$OV_PASS\" }" /tmp/clouds4.yaml > /tmp/clouds5.yaml
cp /tmp/clouds5.yaml clouds.yaml
cp clouds.yaml secure.yaml

ansible localhost -m lineinfile -b -a "path=/etc/hosts regexp='fdp.nfv' state=absent"
ansible localhost -m lineinfile -b -a "path=/etc/hosts line='${FIP} api.fdp.nfv'"
ansible localhost -m lineinfile -b -a "path=/etc/hosts line='${FIP} oauth-openshift.apps.fdp.nfv'"
ansible localhost -m lineinfile -b -a "path=/etc/hosts line='${FIP} *.apps.fdp.nfv'"

openstack quota set admin --ram -1 --cores -1

set +e
NODES=0
export KUBECONFIG=$HOME/sos-fdp/build/auth/kubeconfig
oc get nodes
if [[ "$?" == "0" ]]; then
    NODES=$(oc get nodes -o json | jq .items | jq length)
fi
if [[ "$NODES" -lt "3" ]]; then
    ./fdp.sh deploy cluster
fi
set -e

export KUBECONFIG=$HOME/sos-fdp/build/auth/kubeconfig
NODES=$(oc get nodes -o json | jq .items | jq length)
if [[ "$NODES" -lt "3" ]]; then
    echo "Maser nodes not available, exiting..."
    exit 1
fi

sudo dnf install -y nmap

WORKERS=$(oc get nodes --selector node-role.kubernetes.io/worker= --selector node-role.kubernetes.io/master!= -o json | jq -r '.items[] | select(.status.conditions[] | select(.type=="Ready" and .status=="True")) | .metadata.name' | wc -l)
if [[ "$WORKERS" -lt "1" ]]; then
    ./fdp.sh deploy workers 1
    ./csr-approve.sh 1

    oc label mcp worker machineconfiguration.openshift.io/role=worker
    ./fdp.sh install-manifests

    oc apply -f deploy/performance-add-on.yaml
    oc apply -f deploy/sriov-sub.yaml

    # TODO(skramaja): Implement wait for performanceprofile crd
    # TODO(skramaja): Implement wait for sriovoperatorconfig default resource
    sleep 180

    oc apply -f deploy/performance-profile.yaml
    oc patch  schedulers.config.openshift.io cluster --type=merge -p '{"spec":{"mastersSchedulable":false}}'
    oc patch sriovoperatorconfig default --type=merge -n openshift-sriov-network-operator --patch '{"spec":{"enableOperatorWebhook":false}}'
    ./fdp.sh label-nodes
    ./setpolicy.sh
    oc create namespace example-cnf
    oc apply -f deploy/sriovnetwork-uplink.yaml -f deploy/sriovnetwork-downlink.yaml
fi
