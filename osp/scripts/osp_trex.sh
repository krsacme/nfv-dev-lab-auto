#!/bin/bash

set -ex

RUN=${1:-"1"}

OPT="-o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null"

scp $OPT osp/scripts/osp_trex_remote.sh root@${server}:/root/
CMD="bash /root/osp_trex_remote.sh ${server} ${RUN}"
ssh $OPT root@${server} "echo ${CMD}>>/root/auto-cmd-history"
ssh $OPT root@${server} ${CMD}
