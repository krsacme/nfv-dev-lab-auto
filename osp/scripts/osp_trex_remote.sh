#!/bin/bash

set -ex

SERVER=${1}
RUN=${2:-"1"}

dnf install patch -y

rm -rf ~/post-deployment-validate
git clone -b fix_sriov_network --recurse-submodules https://github.com/krsacme/post-deployment-validate.git
cd ~/post-deployment-validate/ansible-nfv
# Patch ansible-nfv till https://review.gerrithub.io/c/redhat-openstack/ansible-nfv/+/516835 is merged
curl -4 https://review.gerrithub.io/changes/redhat-openstack%2Fansible-nfv~516835/revisions/5/patch?download | base64 -d | patch -p1
sed -i "s/accessIPv4/private_v4/g" $(grep -RnH accessIPv4 . -l)

cd ~/post-deployment-validate
python3 -m venv .venv
source .venv/bin/activate

pip install --upgrade pip
pip install setuptools_rust wheel rust>=1.41.0
pip install -r requirements.txt
sudo alternatives --set python /usr/bin/python3

ansible-playbook initialize.yaml -e host=${SERVER} -e user=root -e ssh_key=~/.ssh/id_rsa -e setup_type=virt

curl -o /tmp/nfv_tuned_rhel76.qcow2 http://file.tlv.redhat.com/~vkhitrin/nfv_tuned_rhel76.qcow2
ansible undercloud-0 -m copy -a "src=/tmp/nfv_tuned_rhel76.qcow2 dest=/tmp/nfv_tuned_rhel76.qcow2"

if [[ "${RUN}" == "1" ]]; then
    SUITE_ARGS="--extra @user-specific/trex.yaml"
    PERF_ARGS=""
else
    SUITE_ARGS="--extra @user-specific/trex-only.yaml"
    PERF_ARGS="-e launch_trex=false -e binary_search=false"
fi

ansible-playbook -i inventory suite/trex.yml ${SUITE_ARGS} -vv
ansible-playbook -i inventory ansible-nfv/playbooks/packet_gen/trex/performance_scenario.yml -e setup_os_env=false --extra @debug.trexvar.yml -e cloud_resources=create ${PERF_ARGS} -vv
