#!/bin/bash

KUBECONFIG_REMOTE=/home/stack/sos-fdp/build/auth/kubeconfig
mkdir -p ~/.kube
OPT="-o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null"
scp ${OPT} stack@undercloud-0:${KUBECONFIG_REMOTE} ~/.kube/config

OCP=4.7
if [ ! -f /usr/local/bin/oc ]; then
    wget -q --retry-connrefused --waitretry=1 --read-timeout=20 --timeout=15 -t 0 https://mirror.openshift.com/pub/openshift-v4/clients/ocp/stable-${OCP}/openshift-client-linux.tar.gz
    tar xvf openshift-client-linux.tar.gz && sudo mv kubectl oc /usr/local/bin
    rm -rf openshift-client-linux.tar.gz
fi

ENTRY=($(ssh ${OPT} stack@undercloud-0 "cat /etc/hosts | grep fdp"))
if ! grep -q "${ENTRY[0]}" /etc/hosts; then
    echo "${ENTRY[0]} ${ENTRY[1]}">>/etc/hosts
fi

UP_SUFFIX="uplink"
DN_SUFFIX="downlink"

PORT=($(ssh $OPT stack@undercloud-0 "source overcloudrc; openstack port list " | grep trex | grep ${UP_SUFFIX} | awk '{ print $4 }'))
TREX_MAC_UP=$(ssh $OPT stack@undercloud-0 "source overcloudrc; openstack port show ${PORT} -c mac_address -f value")

PORT=($(ssh $OPT stack@undercloud-0 "source overcloudrc; openstack port list " | grep trex | grep ${DN_SUFFIX} | awk '{ print $4 }'))
TREX_MAC_DN=$(ssh $OPT stack@undercloud-0 "source overcloudrc; openstack port show ${PORT} -c mac_address -f value")

pip3 install ansible openshift kubernetes
if [ -d ~/nfv-example-cnf-deploy ]; then
    rm -rf ~/nfv-example-cnf-deploy
fi
git clone -b sos https://github.com/rh-nfv-int/nfv-example-cnf-deploy.git
cd nfv-example-cnf-deploy
ansible-playbook -vv create.yaml -e @cluster-specific-params-sos.yaml -e trex_mac_str="${TREX_MAC_DN},${TREX_MAC_UP}"

POD=$(oc -n example-cnf get pods -l example-cnf-type=cnf-app -o custom-columns=POD:.metadata.name --no-headers)
if [ -z $POD ]; then
    echo "Pod not found"
    exit 1
fi
