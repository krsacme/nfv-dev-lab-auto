## Scripts used by OSP Jenkins JOB

There are two types of file in this folder:

* Scripts invoked by the Jenkins JOB within Jenkins workspace folder
  File naming format followed:
```
<JOB_NAME>_<STAGE_NAME>.sh
```

* Scripts invoked on the hypervisor via ssh (remote)
  File naming format followed:
```
<JOB_NAME>_<STAGE_NAME>_remote.sh
```
