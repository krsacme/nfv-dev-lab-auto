#!/bin/bash

set -ex

RUN=${1:-"0"}

OPT="-o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null"

if [[ "${RUN}" == "0" ]]; then
    # This section of the code runs in the jenkins server

    # Copy remote script to undercloud
    scp -o ProxyCommand="ssh -W %h:%p root@${server} -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null" -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null osp/scripts/ocp_deploy_remote.sh stack@undercloud-0:~

    # Copy this script to hypervisor
    scp $OPT osp/scripts/ocp_deploy.sh root@${server}:/root/
    CMD="/root/ocp_deploy.sh 1 && exit"
    ssh $OPT root@${server} "echo ${CMD}>>/root/auto-cmd-history"
    ssh $OPT root@${server} ${CMD}
    exit 0
else
    # This section of the code runs in the hypervisor server
    ssh $OPT stack@undercloud-0 "/home/stack/ocp_deploy_remote.sh 2>&1 | tee openshift_install.log"
    exit 0
fi
