#!/bin/bash

set -ex


cd ~/post-deployment-validate
source .venv/bin/activate

# Get CNF mac addressess
KUBECONFIG_REMOTE=/home/stack/sos-fdp/build/auth/kubeconfig
mkdir -p ~/.kube
OPT="-o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null"
scp ${OPT} stack@undercloud-0:${KUBECONFIG_REMOTE} ~/.kube/config

OCP=4.7
if [ ! -f /usr/local/bin/oc ]; then
    wget -q --retry-connrefused --waitretry=1 --read-timeout=20 --timeout=15 -t 0 https://mirror.openshift.com/pub/openshift-v4/clients/ocp/stable-${OCP}/openshift-client-linux.tar.gz
    tar xvf openshift-client-linux.tar.gz && sudo mv kubectl oc /usr/local/bin
    rm -rf openshift-client-linux.tar.gz
fi

ENTRY=($(ssh ${OPT} stack@undercloud-0 "cat /etc/hosts | grep fdp"))
if ! grep -q "${ENTRY[0]}" /etc/hosts; then
    echo "${ENTRY[0]} ${ENTRY[1]}">>/etc/hosts
fi

function getMac() {
    local net_name="$1"
    POD=$(oc -n example-cnf get pods -l example-cnf-type=cnf-app -o custom-columns=POD:.metadata.name --no-headers)
    RES0_NAME=$(oc -n example-cnf get cnfappmac $POD --no-headers -o custom-columns=:.spec.resources[0].name)
    RES1_NAME=$(oc -n example-cnf get cnfappmac $POD --no-headers -o custom-columns=:.spec.resources[1].name)
    if [[ "${net_name}" == "${RES0_NAME}" ]]; then
        echo "$(oc -n example-cnf get cnfappmac $POD --no-headers -o custom-columns=:.spec.resources[0].devices[0].mac)"
    elif [[ "${net_name}" == "${RES1_NAME}" ]]; then
        echo "$(oc -n example-cnf get cnfappmac $POD --no-headers -o custom-columns=:.spec.resources[1].devices[0].mac)"
    fi
}

UP_SUFFIX="uplink"
DN_SUFFIX="downlink"
DMAC_UP=$(getMac "sriov-${UP_SUFFIX}")
DMAC_DN=$(getMac "sriov-${DN_SUFFIX}")
if [ -z $DMAC_UP ]; then
    echo "Destination mac is not valid"
    exit 1
fi

PORT=($(ssh $OPT stack@undercloud-0 "source overcloudrc; openstack port list " | grep trex | grep ${UP_SUFFIX} | awk '{ print $4 }'))
SMAC_UP=$(ssh $OPT stack@undercloud-0 "source overcloudrc; openstack port show ${PORT} -c mac_address -f value")
NETWORK=$(ssh $OPT stack@undercloud-0 "source overcloudrc; openstack port show ${PORT} -c network_id -f value")
VLAN_UP=$(ssh $OPT stack@undercloud-0 "source overcloudrc; openstack network show ${NETWORK} -c provider:segmentation_id -f value")

PORT=($(ssh $OPT stack@undercloud-0 "source overcloudrc; openstack port list " | grep trex | grep ${DN_SUFFIX} | awk '{ print $4 }'))
SMAC_DN=$(ssh $OPT stack@undercloud-0 "source overcloudrc; openstack port show ${PORT} -c mac_address -f value")
NETWORK=$(ssh $OPT stack@undercloud-0 "source overcloudrc; openstack port show ${PORT} -c network_id -f value")
VLAN_DN=$(ssh $OPT stack@undercloud-0 "source overcloudrc; openstack network show ${NETWORK} -c provider:segmentation_id -f value")

cat<<EOF>port-info.yaml
trex_port_info:
  - src_mac: ${SMAC_UP}
    dest_mac: ${DMAC_UP}
    vlan: ${VLAN_UP}
  - src_mac: ${SMAC_DN}
    dest_mac: ${DMAC_DN}
    vlan: ${VLAN_DN}

trex_platform:
  master_thread_id: 0
  latency_thread_id: 1
  dual_if:
    - socket: 0
      threads: [2, 3, 4, 5]

trex_process_threads: 4

traffic_cmd: "cd /opt/trafficgen && git checkout . && git fetch -v --all && git rebase origin master && /opt/trafficgen/binary-search.py --traffic-generator trex-txrx --frame-size 64 --max-loss-pct 0.0 --send-teaching-warmup --num-flows 1  --use-src-ip-flows 1 --use-dst-ip-flows 1 --use-src-mac-flows 0 --use-dst-mac-flows 0 --use-src-port-flows 1 --use-dst-port-flows 1 --validation-runtime 30 --search-runtime 30 --no-promisc --validation-runtime 10 --search-runtime 10 --dst-macs ${DMAC_UP},${DMAC_DN} --output-dir /opt/ --vlan-ids ${VLAN_DN},${VLAN_UP} --measure-latency 0 --use-device-stats --rate-unit mpps --rate 5"
EOF

ansible-playbook -i inventory ansible-nfv/playbooks/packet_gen/trex/performance_scenario.yml --extra @debug.trexvar.yml -e cloud_resources=create -e dut_macs="${DMAC_UP},${DMAC_DN}" -e @port-info.yaml -vv

FIP_PREFIX=172
FIP=$(ansible undercloud -m os_server_facts -a "cloud=overcloud server=trex" | grep '"addr":' | grep ${FIP_PREFIX} | cut -d ':' -f 2 | cut -d ',' -f 1 | sed 's/"//g' | xargs)

set +e
sudo dnf install jq -y
ssh $OPT -i /tmp/test_keypair.key cloud-user@${FIP} "sudo chmod 777 /opt/binary-search.json"
scp $OPT -i /tmp/test_keypair.key cloud-user@${FIP}:/opt/binary-search.json .
jq ".trials[-1].rate" binary-search.json
jq ".trials[-1].rate" binary-search.json > ~/binary-search-output
