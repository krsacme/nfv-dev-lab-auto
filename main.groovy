import jenkins.*
import jenkins.model.*

import com.cloudbees.hudson.plugins.folder.*

println "JOB_NAME is ${JOB_NAME}"
println "WORKSPACE is ${WORKSPACE}"

println "Find URL..."
def repo_dir = new StringWriter()
def error = new StringWriter()
def err = 0
def cmd = ["/bin/sh", "-c", "cd ${WORKSPACE} ; git ls-remote --get-url"]
cmd.execute().with{
  it.waitForProcessOutput(repo_dir, error)
  if (it.exitValue() != 0) {
    err = 1
    println "error=$error"
    println "code=${it.exitValue()}"
    return
  }
}
if (err == 1) {
  return
}
println "Repo is $repo_dir"

def branch_str = new StringWriter()
def cmd2 = ["/bin/sh", "-c", "cd ${WORKSPACE} ; git name-rev \$(git rev-parse HEAD)"]
cmd2.execute().with{
  it.waitForProcessOutput(branch_str, error)
  if (it.exitValue() != 0) {
    err = 1
    println "error=$error"
    println "code=${it.exitValue()}"
    return
  }
}
if (err == 1) {
  return
}
def branch_name = branch_str.toString().split("/")[-1]
println "Branch is $branch_name"

def dir_name = "OSP-Setup"
if (JOB_NAME != "import_job") {
  dir_name = JOB_NAME + "_OSP_Setup"
}
def dir1 = new File(__FILE__)
def dir = new File(dir1.getAbsoluteFile().getParent()+'/jobs/')

folder(dir_name) {
    displayName(dir_name)
    description('Folder for project OSP Setup')
}

dir.eachFileMatch(~/.*\.yml/)  {
  def jobName = it.name[0..(it.name.lastIndexOf('.')-1)]
  pipelineJob(dir_name + '/' + jobName) {
    def repo = repo_dir.toString().trim()
    def temp = branch_name
    definition {
      cpsScm {
        scm {
          git {
            remote {
              credentials('jenkins')
              url(repo)
              refspec(temp)
            }
          }
        }
        scriptPath('jobs/'+jobName+'.yml')
      }
    }
  }
}
